require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @valid = users(:validUser)
    @noUsername = users(:noUsername)
    @noEmail = users(:noEmail)
  end

  test 'valid user' do
    assert @valid.valid?
  end

  test 'invalid without username' do
    assert_not_nil @noUsername.errors[:username], 'user was valid without a username'
  end

  test 'invalid without email' do
    assert_not_nil @noUsername.errors[:username], 'user was valid without an email'
  end
end