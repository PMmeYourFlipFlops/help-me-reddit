Rails.application.routes.draw do
  
  # Post routes
  get '/posts', to: 'posts#index'
  get '/posts/:id', to: 'posts#show'

  # User routes
  get '/users', to: 'users#index'
  get '/users/:id', to: 'users#show'
  post '/signup', to: 'users#create'
  delete '/users/:id', to: 'users#destroy'
  put '/users/:id', to: 'users#update'

  # Comment routes
  get '/comments', to: 'comments#index'
  get '/comments/:id', to: 'comments#show'

  # Session route
  post '/login', to: 'auth#login'
  get '/profile', to: 'users#profile'

end