class UsersController < ApplicationController
    before_action :get_user, only: [:show, :update, :destroy]

    def profile
        render json: @current_user
    end

    def index
        @users = User.all
        render json: @users
    end

    def show
        render json: @user
    end

    def create
        @user = User.create(user_params)
        if @user.valid?
            render json: {token: create_token(user.id, user.username, user.email)}
        else
            render json: {errors: @user.errors.full_messages}, status: 422
        end
    end

    def update
        if @user.update(user_params)
            render json: @user
        else
            render json:{errors: @user.errors.full_messages}, status: 422
        end
    end

    def destroy
        @user.destroy
    end

    private

    def get_user
        @user = User.find(params[:id])
    end

    def user_params
        params.require(:user).permit(:username, :name, :email, :password)
    end

end