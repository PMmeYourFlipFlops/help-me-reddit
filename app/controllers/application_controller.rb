class ApplicationController < ActionController::API

    def token
        request.headers["Authorization"].split(" ")[1]
    end

    def secret
        ENV['jwt_secret']
    end

    def decoded_token
        JWT.decode(token, secret, true, { algorithm: 'HS256' })
    end

    def current_user
        @current_user ||= User.find(decoded_token[0]["id"])
    end

    def create_token(id, username, email)
        payload = { id: id, username: username, email: email }
        JWT.encode(payload, secret, 'HS256')
    end

end