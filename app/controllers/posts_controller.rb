class PostsController < ApplicationController
    before_action :get_post, only: [:show, :update, :destroy]

    def index
        @posts = Post.all
        render json: @posts
    end

    def show
        render json: @post
    end

    def create
        @post = Post.create(post_params)
        render json: @post
    end

    def update
        if @post.update(post_params)
            render json: @post
        else
            render json: {errors: @post.errors.full_error_messages}, status: 422
        end
    end

    def destroy
        @post.destroy
    end

    private
    def get_post
        @post = Post.find(params[:id])
    end

    def post_params
        params.require(:post).permit(:title, :body, :user_id)
    end

end
