class User < ApplicationRecord
    has_secure_password
    validates :username, presence: true
    validates :name, presence: true
    validates :email, presence: true

    has_many :posts
    has_many :comments, through: :posts
    has_many :featured_image, through: :posts
end