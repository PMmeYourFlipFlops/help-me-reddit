class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :username, :email, :posts, :comments

  def posts
    object.posts.size
  end

  def comments
    object.comments.size
  end

end