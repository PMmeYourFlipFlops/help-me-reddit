class PostSerializer < ActiveModel::Serializer
  # belongs_to :user
  include Rails.application.routes.url_helpers
  attributes :id, :user_id, :title, :body, :comments, :featured_image

  def comments
    object.comments.size
  end

  def featured_image
    if object.featured_image.attached?
      {
        url: rails_blob_url(object.featured_image)
      }
    end
  end
end